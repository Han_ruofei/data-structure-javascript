/*
  实现思路：
  先将每次传递的参数都放在原型上
*/
function sum (...args) {
  return reduce((pre, cur) => {
    return pre + cur
  })
}
function currying (func) {
  const args = []
  return function result (...rest) {
    if (rest.length === 0) {
      return func(...args)
    }
    else {
      args.push(...rest)
      return result
    }
  }

}

console.log(currying(sum)(1, 2)(3)(4, 5, 6)())