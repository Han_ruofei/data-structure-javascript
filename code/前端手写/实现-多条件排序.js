const sortData = (data) => {
  return data.sort((a, b) => {
    // 提取a和b的数据序号，并转换为整数  
    const serialA = parseInt(a.substring(1), 10)
    const serialB = parseInt(b.substring(1), 10)
    // 如果数据序号不同，则按数据序号排序  
    if (serialA !== serialB) {
      return serialA - serialB
    }
    // 如果数据序号相同，则按数据类型排序  
    return a.charCodeAt(0) - b.charCodeAt(0)
  })
}


// 输出排序后的结果  
console.log(sortData(["B3", "D2", "F1", "A9", "D12", "A2", "C1", "Z0", "B1"]))