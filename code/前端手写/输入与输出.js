
const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
// 单行输入
// rl.on('line', function (data) {
//   var result = data.trim().split(' ')
//   console.log(result)
// })

// 固定行数的多行输入
// let k = 3   //行数
// const datas = []   //用于存储用户输入的数据
// rl.on('line', function (data) {
//   datas.push(data.trim())
//   if (datas.length === k) {
//     console.log(datas)
//   }
// })

// 第一次输入的数据说明了接下来要输入几行
let k = -1  //为-1表示还未开始
const rows = []
rl.on('line', function (data) {
  if (k < 0) {
    k = parseInt(data.trim().split(' ').at(-1))
    console.log('k是', k, typeof (k))
  }
  else {
    rows.push(data.trim())
    if (k == rows.length) {
      console.log(rows)
    }
  }
})