// 类型测试
function typeOf (obj) {
  // return Object.prototype.toString.call(obj)
  // 返回的类型为String类型 '[Object Array]'
  // 将正确的数据类型进行裁剪，slice支持负数裁剪
  // 可以判断null，undefined，function，Array等类型 结果是 Null，Undefined，Function，Array
  return Object.prototype.toString.call(obj).slice(8, -1)
}
let a = ['zs', 18, null, undefined, Symbol(3), true, new Date(), BigInt(1232323232323), { name: 'zs' }, [12, 3, 4, 3, 5], function () { console.log('test') }]
a.forEach(item => {
  try {
    console.log(typeOf(item))
  }
  catch (e) {
    console.log(e)
  }
})