/*
  写一个函数A
  要求：1. 变量x=0,每一秒增加1,切输出x的值，当x>3时，循环停止后输出 'in clear'
        2. 当A函数同时被调用3次时，输入结果为 x=1,x=2,x=3 in clear

*/
function getX () {
  let x = 1
  let flag = true
  return function fn () {
    if (x < 4) {
      setTimeout(() => {
        // flag = false
        console.log(x++)
      }, 1000)
    }
    else {
      console.log('in clear')
    }
  }
}
let A = getX()
A()
A()
A()