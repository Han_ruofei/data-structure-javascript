function getMaxAndSecond (arr) {
  if (arr.length < 2) return
  const result = []
  arr = [...new Set(arr)]
  let max = arr[0], second = arr[1]
  for (let i = 0; i < arr.length; i++) {
    // 当前元素大于second 大于max
    if (arr[i] > second && arr[i] > max) {
      second = max
      max = arr[i]
    }
    // 当前元素大于second 小于max
    if (arr[i] > second && arr[i] < max) second = arr[i]
  }

  return [max, second]
}

console.log(getMaxAndSecond([10, 4, 8, 10, 9, 2, 6]))