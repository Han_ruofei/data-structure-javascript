/*
instanceof 的使用
function Person(){}
let p = new Person()
p instanceof Person   
>true
主要就是判断左侧的proto是否能在右侧的protype的yuanxing上找到
*/

// function myInstanceof (leftValue, rightValue) {
//   let rightProto = rightValue.prototype
//   leftValue = leftValue.__proto__
//   while (true) {
//     if (rightProto === leftValue) {
//       return true
//     }
//     if (leftValue === null) {
//       return false
//     }
//     leftValue = leftValue.__proto__
//   }
// }
const myInstanceof2 = (left, right) => {
  // 获取对象的原型
  let proto = Object.getPrototypeOf(left)
  const prototype = right.prototype
  while (1) {
    if (proto === prototype) return true
    if (!proto) return false
    proto = Object.getPrototypeOf(proto)
  }
}
function Person () { }
let p = new Person()
class Persons { }
console.log(myInstanceof2(p, Person))
console.log(myInstanceof2(p, Persons))
console.log(p instanceof Person)
console.log(p instanceof Persons)