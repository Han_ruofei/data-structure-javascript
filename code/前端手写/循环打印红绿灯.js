// 初始化打印红灯，3秒后变成路灯，5秒后变成黄灯，2秒后变成红灯
// function red () {
//   return new Promise((resolve) => {
//     console.log('当前是红灯,3秒后变成绿灯')
//     setTimeout(() => {
//       const geeenPromise = geeen()
//       resolve(geeenPromise)
//     }, 3000)
//   })
// }
// //绿灯
// function geeen () {
//   return new Promise((resolve) => {
//     console.log('当前是绿灯,5秒后变成黄灯')
//     setTimeout(() => {
//       const yellowPromise = yellow()
//       resolve(yellowPromise)
//     }, 3000)
//   })
// }
// //黄灯
// function yellow () {
//   return new Promise((resolve) => {
//     console.log('当前是黄灯,2秒后变成红灯')
//     setTimeout(() => {
//       const redPromise = red()
//       resolve(redPromise)
//     }, 3000)
//   })
// }
// red()
//封装公共方法
function timer (color, delay, next) {
  return new Promise((resolve) => {
    console.log(`当前是${color}灯，${delay}秒后变成${next}灯`)
    setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

async function light () {
  await timer('红', 3, '绿')
  await timer('绿', 1, '黄')
  await timer('黄', 5, '红')
  await light()
}
light()
