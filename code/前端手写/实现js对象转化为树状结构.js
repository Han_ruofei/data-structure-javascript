let source = [{
  id: 1,
  pid: 0,
  name: 'body'
}, {
  id: 2,
  pid: 1,
  name: 'title'
}, {
  id: 3,
  pid: 2,
  name: 'div'
}]
function jsonToTree (data) {
  if (!Array.isArray(data)) {
    return result
  }
  // 1. result为一个新的大厅，来存放已经组好队的家庭
  let result = []
  // 2.使用map，将当前对象的id与当前对象对应存储起来
  // PS:相当于我们在大厅喊话，只用一次就可以找到父亲，也就是O(1)
  let map = {}
  data.forEach(item => {
    map[item.id] = item
  })
  // 3.询问每一个人，它的父亲是谁
  data.forEach(item => {
    let parent = map[item.pid]
    console.log('打印的parent', parent)
    // 如果有父亲，就把父亲喊出来，把孩子交给父亲
    if (parent) {
      (parent.children || (parent.children = [])).push(item)
    }
    else {
      // 如果没有父亲,说明这个人就是父亲,直接把他带到排好序的大厅
      result.push(item)
    }

  })
  // 新的大厅为已经排好的家庭
  return result
}
console.log(jsonToTree(source))