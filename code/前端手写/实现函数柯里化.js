/*
步骤：
不定参数的函数柯里化，实现sum求和的功能
因为每次传参不知道下次还有没有，
因此我们采用闭包的形式将所有参数保存起来，
之后在每次调用该函数时就判断是否有参数
如果有参数就将参数传到闭包中进行存储，
如果没有参数则说明是调用
调用直接将所有参数加在一起即可
*/
// ...args是剩余参数的写法，将不定参数以数组的形式传递
/*
可以再回顾一下reduce的使用方法
reduce数组方法，接收两个参数，第一个为一个累加器函数，第二个参数为初始值
比如对数组累加，我们如果不设置初始值，也就是第二个参数，那么累加器中
第一个参数pre就是数组的第一个元素，cur就是第二个元素，也就是第一次就会访问到
数组的第一个和第二个元素
如果传递了初始值，那么数组索引就会从0开始，也就是第一个cur是数组第一个元素，
pre为传递的初始值
*/
function add (...args) {
  return args.reduce((pre, cur) => pre + cur)
}
function currying (fn) {
  // 作为参数存储的容器
  let argArr = []
  // 中间容器，用来判断传入是否有传入参数
  return function temp (...arg) {
    // 只要传入的参数不为0，就继续将参数交给容器
    if (arg.length) {
      // 使用扩展运算符,将result中的值进行更新
      argArr = [...argArr, ...arg]
      return temp
    }
    // 如果没有参数的话则说明是最后一个括号，也就是执行
    else {
      let val = fn(...argArr)
      argArr = []
      return val
    }
  }
}

let sum = currying(add)
console.log(sum(1, 2, 3, 4)(1)())  //15
// console.log(addCurry(1)(2)(3, 4, 5)())  //15
// console.log(addCurry(1)(2, 3, 4, 5)())
