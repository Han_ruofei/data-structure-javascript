

function wordAndweight (str) {
  if (!str) return null
  // 先对str按照空格进行切割 并统计其次数
  let result = str.split(' ').reduce((pre, item) => {
    pre[item] ? pre[item]++ : pre[item] = 1
    return pre
  }, {})
  // 统计权重
  let weightArr = Object.entries(result).map(item => {
    item[2] = item[0].length * item[1]
    return item
  })
  // 按照权重对其进行排序
  weightArr.sort((a, b) => {
    return b[2] - a[2]
  })
  return `${weightArr[0][0]} ${weightArr[0][1]}`
}
let str = 'English is very important, but my English is very poor is is is is is is is is is is is is is is is is is is'
let result = wordAndweight(str)
console.log(result)
