function promiseAll (promiseArr) {
  return new Promise((resolve, reject) => {
    const len = promiseArr.length
    const result = []
    let count = 0
    for (let i = 0; i < len; i++) {
      Promise.resolve(promiseArr[i])
        .then((res) => {
          result[i] = res
          if (++count === len) {
            resolve(result)
          }
        }).catch((e) => {
          reject(e)
        })
    }
  })
}

function testFun (sec) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(sec)
    }, sec)
  })
}
const result = promiseAll([testFun(1), testFun(2), testFun(3)])
console.log(result)
