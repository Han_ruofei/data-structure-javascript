let obj = {
  name: 'zs',
  age: 14
}
// 使用属性添加
obj.hobby = 'eat'
// 使用Object.defineProperty
Object.defineProperty(obj, 'sport', {
  value: 'badminton',     //设置默认值
  enumerable: true,       //是否可枚举，也就是我们在遍历时 是否能够遍历出属性
  configurable: false,    // 属性是否可删除
  writable: true,         //是否可写
  // get: function () {     //访问元素时就会调用  如果自定义get和set的话，那么就不能出现writable和value，否则会报错
  //   console.log(this)
  //   return 'nu666m'
  // },
  // set (value) {      //设置元素时就会调用   如果自定义get和set的话，那么就不能出现writable和value，否则会报错
  //   this.sport = value
  // }
})
/*
!!!
如果自定义get和set的话，那么就不能出现writable和value，否则会报错

*/

// Object.prototype.love = 3
// let num = 666
// // obj.sport = 'basketabll'
// console.log(obj.sport)
// num = 999
delete obj.sport
console.log(obj.sport)
// for (let i in obj) {
//   if (obj.hasOwnProperty(i)) {
//     console.log(i)
//   }
// }