const arr = [
  { value: '1' },
  { value: 'html' },
  { value: 3 },
  { value: '2' },
  { value: 'css' },
  { value: '中文' }
]
const a = [], b = []
// 遍历数组元素，元素的value符合条件，则添加到a，否则添加到b
arr.forEach(item => {
  if ((typeof (item.value) == 'number') || (!isNaN(Number(item.value)))) {
    a.push(item.value)
  }
  else {
    b.push(item.value)
  }
})
console.log(a, b)