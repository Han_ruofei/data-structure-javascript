const arr = [2, 2, 1, 1, 1, 2, 2]
let result = arr.reduce((pre, item) => {
  pre[item] ? pre[item]++ : pre[item] = 1
  return pre
}, {})
console.log(result)
// 按照对象属性值进行排序
let keyArr = Object.keys(result).sort(function (a, b) {
  return result[b] - result[a]
})
console.log(keyArr[0], typeof (keyArr[0]))