function Animal () {
  this.colors = ['black', 'white']
}
Animal.prototype.getColor = function () {
  return this.colors
}
function Dog () { }
Dog.prototype = new Animal()

// let dog1 = new Dog()
// dog1.colors.push('brown')
let dog2 = new Dog()
console.log(dog2.colors)
// 缺点 ：圆形中包含的引用类型属性将被所有实例共享，子类不能传参

