// 1.不去关心回调函数是否还在运行
// 在某些情况下，函数可能需要比间隔时间更长的时间去完成执行。
//比如说是用setInterval每隔5秒对远端服务器进行轮询，网络延迟，服务器无响应以及其他因素将会阻止请求按时按成。结果会导致返回一串无必要的排成队列请求。

// 2.忽视错误
// 因为某些原因，setInterval调用的代码中会出现一个错误，但是代码并不会中止执行而是继续执行错误的代码。

// 3.缺乏灵活性
// 除了前面提到的缺点之外，我非常希望setInterval方法能有一个表明执行次数的参数而不是无休止的执行下去。
function interval (func, w, t) {
  const interv = function () {
    t && console.log(`还剩${t}次`)
    if (typeof t === "undefined" || t-- > 0) {
      setTimeout(interv, w)
      try {
        func()
      }
      catch (e) {
        t = 0
        throw e.toString()
      }
    }
  }
  setTimeout(interv, w)
}
function func () {
  console.log('开始执行了')
}
// interval(func, 3000, 5)

function mySetInterval (fn, wait, times) {
  // 确保fn是函数，wait是数字  
  if (typeof fn !== 'function' || typeof wait !== 'number' || isNaN(wait)) {
    throw new Error('Invalid arguments for mySetInterval')
  }

  // 定义一个递归函数来模拟setInterval  
  const interval = () => {
    try {
      // 执行回调函数  
      // 如果没有达到执行次数，或者没有设置执行次数，继续执行  
      if (times === 'undefined' || --times > 0) {
        setTimeout(interval, wait)
      }
      fn()
    } catch (error) {
      // 如果回调函数抛出错误，停止执行  
      console.error('An error occurred in the callback function:', error)
      clearInterval(timer) // 清除可能存在的timer  
    }
  }

  // 开始执行  
  interval()
}



// 使用示例 - 执行5次
mySetInterval(() => {
  console.log('This will run every second, 5 times.')
}, 1000, 5)