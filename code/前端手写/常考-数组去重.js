/*
  去除数组中的重复元素
  方法1：
  使用set
*/
function useSet (arr) {
  return Array.from(new Set(arr))
}
/*
  方法2：
  使用for循环+splice进行数组去重
  需要注意，删除完之后进行j--
*/
function useSplice (arr) {

  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        arr.splice(j, 1)
        j--
      }
    }
  }
  return arr
}
/*
  方法3：
  使用indexOf去重
*/
function useIndexof (arr) {
  const newArr = []
  if (!Array.isArray(arr)) return false
  // indexOf如果有则返回第一个匹配到的位置，如果没有匹配到则返回-1
  for (let i = 0; i < arr.length; i++) {
    if (newArr.indexOf(arr[i]) === -1) {
      newArr.push(arr[i])
    }
  }
  return newArr
}
/*
  方法4：
  先进行排序，排序之后判断前后两个是否相等
*/
function useSort (arr) {
  arr.sort((a, b) => { return a - b })
  const newArr = [arr[0]]
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== arr[i - 1]) {
      newArr.push(arr[i])
    }
  }
  return newArr
}

return res
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', {}, {}]
console.log(useIndexof(arr))