// 要求：给函数传递一个毫秒数，过了该时间后返回一个promise
function myPromise (time) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, time)
  })
}
console.log('开始执行了')
let p = myPromise(3000)
p.then(() => {
  console.log('我是then中执行的回调函数')
})
