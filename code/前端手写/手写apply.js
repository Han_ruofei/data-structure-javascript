// 有了之前call的实现,现在对apply的实现也更加容易
// call和apply的差别比较小
// call的第一个参数是更改的this的指向,后面的是函数的参数
// apply的第一个参数是更改的this的指向,后面的参数使用数组进行传递
Function.prototype.myapply = function (context) {
  let args = arguments[1]
  context = context || window
  context.fn = this
  context.fn(...args)
}
let obj = { name: 'zs', age: 12 }

function test () {
  console.log(this.name)
  console.log(arguments)
}
test.myapply(obj, ['aaa', 'bbb', 'ccc'])
// test.apply(obj, ['aaa', 'bbb', 'ccc'])
// test.call(obj, 'aaa', 'bbb', 'ccc')
