// const deepClone = function (obj) {
//   return JSON.parse(JSON.stringify(obj))
// }
function deepClone (obj) {
  if (typeof (obj) !== 'object' || obj === null) return obj
  //对类型进行限制
  let result = Array.isArray(obj) ? [] : {}
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      result[key] = deepClone(obj[key])
    }
  }
  return result
}
let a = {
  name: 'zs',
  age: 12,
  sex: 'male',
  hobby: ['eat', 'sleep', 'play'],
  play: {
    game: {
      name: 'wzry',
      time: 10,
      salary: 1000
    },
    ball: {
      name: 'badminton',
      time: 3,
      money: 1000
    }
  }
}
let b = deepClone(a)
b.ball = ['pingpang', 'basketball']
console.log(a)
console.log(b)
// 深拷贝完整版
const _completeDeepClone = (target, map = new Map()) => {   // map 用来记录是否已经被拷贝过
  // 补全代码
  if (typeof target == "object" && target !== null) {
    if (/^(Function|RegExp|Date|Set|Map)$/i.test(target.constructor.name)) return new target.constructor(target)  //进行深拷贝，不能传递地址
    if (map.get(target)) return map.get(target)                       // 已经遍历到了，直接返回结果
    map.set(target, true)                                             // 还未遍历过，设置为 true
    let collect = Array.isArray(target) ? [] : {}
    for (prop in target) {                                             // 开始遍历
      if (target.hasOwnProperty(prop))
        collect[prop] = _completeDeepClone(target[prop], map)
    }
    return collect
  } else {
    return target
  }
}