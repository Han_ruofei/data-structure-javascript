/*
  之前已经写过了call和apply的实现原理
  bind和之前两个又有所不同
  bind也会改变this的指向
  只不过bind不会立即执行,它会返回一个新的函数
  我们可以基于call和apply来分别实现bind函数
*/
Function.prototype.myapply = function (context) {
  let args = arguments[1]
  context = context || window
  context.fn = this
  context.fn(...args)
  delete context.fn
}
Function.prototype._bind = function (context, ...test) {
  context = context === null || context === undefined ? window : Object(context)
  context.fn = this
  return (...arguments2) => {
    return context.fn(...test, ...arguments2)
  }
}
Function.prototype._bind2 = function (target, ...arguments1) {
  const _this = this
  return function (...arguments2) {
    return _this.apply(target, [...arguments1, ...arguments2])
  }
}

function test (b, c) {
  console.log(this.name + b + c)
}
let obj = {
  name: 'libai'
}
let test3 = test.bind(obj, 'name', 'zs')
test3()
// let test2 = test._bind(obj, 'aaa', 'bbb', 'ccc')

// test2('666')


// Function.prototype.myBind = function () {
//   // 保存bind传入进来的第一个参数
//   const params_first = arguments[0]
//   // 把bind传入的第2,3,4...等参数保存起来
//   const arg = [...arguments].slice(1)
//   // this指向第一个参数，把原函数绑定成该参数的一个属性即可
//   params_first.tempUniqueFunction = this
//   return function () {
//     for (let i = 0; i < arguments.length; i++) {
//       arg.push(arguments[i])
//     }
//     const newFunction = params_first.tempUniqueFunction(...arg)
//     // 不要凭空给参数添加属性，所以用完我们还得删除，因此用newFunction保存了新的this指向的函数
//     delete params_first.tempUniqueFunction
//     return newFunction
//   }
// }
// function test () { }
// console.log(test.myBind()())
