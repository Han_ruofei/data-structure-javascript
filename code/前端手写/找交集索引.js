const getIntersectionIndex = (arr1, arr2) => {
  const result = []
  for (let i = 0; i < arr2.length; i++) {
    const item = arr2[i]
    while (arr1.includes(item)) {
      const index = arr1.indexOf(item)
      arr1.splice(index, 1, null)
      result.push(index)
    }
  }
  return result.sort((a, b) => a - b)
}

const getIntersectionIndex2 = function (arr1, arr2) {
  const result = []
  const map = new Map()
  arr2.forEach(ar2 => {
    map.set(ar2, ar2)
  })
  arr1.forEach((item, index) => {
    if (map.has(item)) result.push(index)
  })
  return result
}

function getIntersectionIndex3 (arr1, arr2) {
  const res = []
  for (let i = 0; i < arr1.length; i++) {
    for (let j = 0; j < arr2.length; j++) {
      if (arr1[i] === arr2[j]) {
        res.push(i)
      }
    }
  }
  return res
}
console.log(getIntersectionIndex([5, 3, 1, 5, 4], [5, 3]))
console.log(getIntersectionIndex2([5, 3, 1, 5, 4], [5, 3]))
console.log(getIntersectionIndex3([5, 3, 1, 5, 4], [5, 3]))