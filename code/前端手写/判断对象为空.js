// 1. 使用JSON.stringify
const obj = {}
console.log(JSON.stringify(obj) === '{}')
// 使用Object.keys
console.log(Object.keys(obj).length === 0)
// 使用Object.values
console.log(Object.values(obj).length === 0)
// 使用Reflect.ownKeys
console.log(Reflect.ownKeys(obj).length === 0)
// 使用Object.getOwnPropertyNames
console.log(Object.getOwnPropertyNames(obj).length === 0)