// 十进制转二进制
const dec2bin = (decNumber, base) => {
  let a = decNumber
  let remainder = 0
  let result = ''
  while (a > 0) {
    remainder = a % base
    result = remainder + result
    a = Math.floor(a / base)
  }
  return result
}
const myParseInt = function (binNumber, base) {
  num = binNumber.toString().split('').reverse()
  let result = 0
  for (let i = 0; i < num.length; i++) {
    const item = parseInt(num[i])
    console.log('result,base,item,item', result, base, item,)
    result += (base ** i * item)
  }
  return result
}

console.log(myParseInt('1010', 16))
// console.log(bin2dec('1010', 2))
// console.log(dec2bin(8, 2))