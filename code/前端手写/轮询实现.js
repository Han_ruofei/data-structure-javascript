// 搜到很多使用setTimout和setInterval的，应该是两者都可以
// 但是才疏学浅 只能看懂setInterval的，大概意思就是每隔几分钟调用查询函数
let flag = false
function checkInfo () {
  console.log('发送请求获取数据中ing……')
  let timer = setTimeout(() => {
    flag = true
    clearTimeout(timer)
  }, 6000)
}
console.log('轮询开始了！')
let timer = setInterval(() => {

  if (flag) {
    console.log('查询到请求成功,轮询结束！')
    clearInterval(timer)
    return
  }
  checkInfo()
  console.log('正在查询,没结束')
}, 1000)