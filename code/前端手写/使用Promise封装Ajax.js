function myAjax (url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
      resolve(this.response)
    }
    xhr.onerror = function () {
      reject(new Error(this.statusText))
    }
    xhr.send()
  })
}
let url = 'http://zx529.xyz:5003/seatSell/hotFilm'
myAjax(url).then(res => {
  console.log(res)
})