/**
 * const arr =
  [{
    id: 1,
    pid: 0,
    name: '爷爷',
    children: [{
      id: 2,
      pid: 1,
      name: '爸爸',
      children: [{
        id: 3,
        pid: 1,
        name: '孙子'
      }]
    }]
  }]
转成
[
  { id: 1, pid: 0, name: '爷爷' },
  { id: 2, pid: 1, name: '爸爸' },
  { id: 3, pid: 1, name: '孙子' }
]
 */

const obj2Arr = (obj) => {
  if (!Array.isArray(obj[0])) return obj
  const result = []
  const currentTree = obj[0]
  const { id, pid, name } = currentTree
  result.push({ id, pid, name })
  const itemChildren = currentTree.children
  if (itemChildren) {
    result.push(...obj2Arr(itemChildren))
  }
  return result

}

const arr =
  [{
    id: 1,
    pid: 0,
    name: '爷爷',
    children: [{
      id: 2,
      pid: 1,
      name: '爸爸',
      children: [{
        id: 3,
        pid: 1,
        name: '孙子'
      }]
    }]
  }]
console.log(obj2Arr(arr))