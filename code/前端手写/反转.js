const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
rl.on('line', function (line) {
  const tokens = line.split(' ')
  myReserve(tokens[0], tokens[1], tokens[2])
})
function myReserve (str, start, end) {
  if (!str.includes(start) && !str.includes(end)) return str
  let myStr = str.split('')
  let startIndex = str.indexOf(start) > -1 ? str.indexOf(start) : 0
  let endIndex = str.indexOf(end) > -1 ? str.indexOf(end) + 1 : str.length
  let resStr = str.slice(startIndex, endIndex).split('').reverse().join('')

  myStr.splice(startIndex, endIndex - startIndex, resStr)
  console.log(myStr.join(''))
}