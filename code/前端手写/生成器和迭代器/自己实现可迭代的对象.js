// for of一般用于遍历数组，for in一般用于遍历对象 常规
// for in遍历时每一项都是索引，当遍历数组时，item都是索引，遍历对象时，每一项都是对象的属性
// 切记:for of不能用来遍历对象，因为for of没有自己的 [Symbol.iterator]()方法


// 手动给对象添加一个 [Symbol.iterator]属性

let obj = {
  values: [1, 3, 5, 3, 6],
  [Symbol.iterator] () {
    let index = -1
    return {
      // 这里如果直接使用this，指向的时当前函数，无法找到values，因此我们可以使用箭头函数或者bind
      next: () => {
        return index === this.values.length - 1
          ? { value: undefined, done: true }
          : { value: this.values[++index], done: false }
      }
    }
  }
}


for (let v of obj) {
  console.log(v)
}
