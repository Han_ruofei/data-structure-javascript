// generator类似状态机，格式入下:
/*
  function* helloWorldGenerator(){
    yield 'hello'
    yield 'world'
    return 'ending'
  }
调用next方法会跳至下一个状态
*/
function* myGen () {
  console.log('start')
  yield '1'
  console.log('now is 1')
  yield '2'
  yield '3'
  console.log('over')
}
let h = myGen()
// 可以看到结果就像我们之前的模拟迭代器的结果是一样的
// 每调用依次next就会执行到一次yield之前的 
console.log(h.next())
console.log(h.next())
console.log(h.next())
console.log(h.next())