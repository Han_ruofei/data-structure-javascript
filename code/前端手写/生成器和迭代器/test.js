const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
let k = -1
const result = []
rl.on('line', function (data) {
  if (k === -1) count = parseInt(data)
  else {
    let res = []
    res.length === 3 ? result.push(res) : res.push(data)
  }
  if (result.length === 3) {
    getResult(result)
  }
})
function getResult (result) {
  for (let i = 0; i < result.length; i++) {
    console.log(isLegal(result[i][1], result[i][2]))
  }
  const isLegal = function (pushStr, popStr) {
    let pushArr = pushStr.split(' ').map(item => parseInt(item))
    let popArr = popStr.split(' ').map(item => parseInt(item))
    if (pushArr.length !== popArr.length) return 'NO'
    let stack = []
    let pushIndex = 0
    let popIndex = 0
    while (pushIndex < pushArr.length) {
      stack.push(pushArr[pushIndex])
      while (stack.length > 0 && stack[stack.length - 1] === popArr[popIndex]) {
        stack.pop()
        popIndex++
      }
      pushIndex++
    }
    return stack.length === 0 ? 'YES' : 'NO'
  }
}