// yield会阻止代码执行

function* gen (x) {
  let a = yield x + 1
  console.log(a)
  let b = yield a + 1
  console.log(b)
  let c = yield b + 1
  return c

}
let generator = gen(1)
let result = generator.next()
// console.log(result)

result = generator.next(100)
// console.log(result)

result = generator.next(5)
// console.log(result)

result = generator.next()
// console.log(result)