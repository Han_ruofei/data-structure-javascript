// 迭代器，每次调用next方法，都会返回一个对象{value:'',done:falst}，done标识后面是否还有元素

const makeIterator = function (arr) {
  let index = -1
  return {
    next () {
      return index === arr.length - 1
        ? { value: undefined, done: true }
        : { value: arr[++index], done: false }
    }
  }
}
let it = makeIterator([1, 24, 4, 2])
// console.log(it.next())
// console.log(it.next())
// console.log(it.next())
// console.log(it.next())
// console.log(it.next())
let result = null
do {
  result = it.next()
  if (!result.done) console.log(result.value)
} while (!result.done)