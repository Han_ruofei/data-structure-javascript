/*
  定义两个指针，一个指向数组头部（left），一个指向尾部（right）
  定义一个指针指向两个指针中间位置mid
  通过判断中间位置的值与target的大小调整指针位置
  1.nums[mid]<target:说明target在mid的右边，此时让left=mid+1
  2.nums[mid]>target:说明target在mid的左边，此时让right=mid-1
  3.nums[mid]=target:找到目标值的下标，直接返回
*/
// const search = (nums, target) => {
// let len = nums.length
// if (!len) return -1
// let [left, right] = [0, len - 1]
// while (left <= right) {
//   // 取中间索引+
//   let mid = left + Math.floor((right - left) / 2)
//   let num = nums[mid]
//   if (num === target) return mid
//   else if (num > target) right = mid - 1
//   else left = mid + 1
// }
// return -1
// }
function search (nums, target) {
  const len = nums.length
  if (!len) return -1
  // 取第一个和最后一个索引
  let [left, right] = [0, len - 1]
  while (left <= right) {
    let midIndex = left + Math.floor((right - left) / 2)
    let midValue = nums[midIndex]
    if (midValue === target) return midIndex
    else if (midValue > target) right = midIndex - 1
    else left = midIndex + 1
  }
  return -1
}
const arr = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11]
console.log(search(arr, 7))