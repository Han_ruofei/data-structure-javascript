/*
  在一个二维数组array中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
  [
  [1,2,8,9],
  [2,4,9,12],
  [4,7,10,13],
  [6,8,11,15]
  ]
  给定 target = 7，返回 true。

  给定 target = 3，返回 false。
*/
// 方法一：遍历整个二维数组，如果有返回true，没有则返回false，
// 方法二：将二维数组拍平，数组扁平化，之后再遍历一次
// 方法三：对每个数组使用一次二分查找

const Find1 = function (target, array) {
  for (let arr of array) {
    for (let ar of arr) {
      if (ar === target) return true
    }
  }
  return false
}
const Find2 = function (target, array) {
  // toString之后再做处理
  //  return array.toString().split(',').map(item => Number(item)).includes(target)
  // 自带的flat方法
  return array.flat().includes(target)
}
function Find (target, array) {
  for (let arr of array) {
    let len = arr.length
    if (!len) {
      // 如果子元素是个空元素，则直接跳过
      continue
    }
    // 二分查找
    let [left, right] = [0, len - 1]
    while (left <= right) {
      let mid = left + Math.floor((right - left) / 2)
      if (target === arr[mid]) return true
      else if (target < arr[mid]) right = right - 1
      else left = left + 1
    }
    console.log('没有找到')

  }
  return false
}
