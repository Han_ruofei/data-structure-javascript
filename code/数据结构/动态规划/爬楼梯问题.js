const climbStep = (n) => {
  const dp = []
  dp[0] = 0
  dp[1] = 1
  dp[2] = 2
  for (let i = 3; i <= n; i++) {
    const i1 = dp[i - 1]
    const i2 = dp[i - 2]
    dp[i] = i1 + i2
  }
  return dp[n]
}

function climbStep2 (n) {
  if (n === 1) return 1
  if (n === 2) return 2
  return climbStep2(n - 1) + climbStep2(n - 2)
}

const climbStep3 = (n) => {
  const dp = []
  let first = 1, second = 2
  for (let i = 3; i <= n; i++) {
    let temp = first + second
    first = second
    second = temp
  }
  return second
}
console.log(climbStep(10))
console.log(climbStep2(10))
console.log(climbStep3(10))