/**
 * 给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。
  子序列 是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。
  例如，[3,6,2,7] 是数组 [0,3,1,6,2,2,7] 的子序列。
 */

// 方法一：暴力递归解法
// const L = (nums，index) {

// }
/**
 * 动态规划
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function (nums) {
  // 定义dp数组，dp[i]表示以nums[i]结尾的最长递增子序列的长度
  const dp = new Array(nums.length).fill(1)
  // 初始化dp数组
  for (let i = 1; i < nums.length; i++) {
    for (let j = 0; j < i; j++) {
      if (nums[i] > nums[j]) {
        dp[i] = Math.max(dp[i], dp[j] + 1)
      }
    }
  }
  // 返回dp数组中的最大值
  return Math.max(...dp)
}
console.log(lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]))

// 