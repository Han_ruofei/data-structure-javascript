const Mirror = function (root) {
  if (!root) return
  const left = root.left
  root.left = root.right
  root.right = left
  Mirror(root.left)
  Mirror(root.right)
  return root
}