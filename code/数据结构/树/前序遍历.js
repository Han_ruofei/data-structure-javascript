const preOrderTraversal = (root) => {
  const result = []
  const dfs = (root) => {
    if (!root) return
    result.push(root.val)
    dfs(root.left)
    dfs(root.right)
  }
  dfs(root)
  return result
}