const isSymmetrical = function (pRoot) {
  // write code here
  if (!pRoot) return true
  const compare = (left, right) => {
    // 如果左子树和右子树都为空 返回true
    if (!left && !right) return true
    // 如果左子树或者右子树有一个为空 返回false
    if (!left || !right) return false
    // 如果左右子树的值不相等 返回false
    if (left.val !== right.val) return false
    // 如果左右子树相等，则继续递归调用，将镜像的节点传递
    return compare(left.left, right.right) && compare(left.right, right.left)
  }
  return compare(pRoot.left, pRoot.right)
}