// 传递过来的每一个节点，我们都要思考，当前节点需要做什么，下一步递归条件是什么，退出条件是什么
function hasPathSum (root, sum) {
  // 递归退出条件
  if (!root) return false
  // 叶子节点，且叶子节点符合要求，即为找到了
  if (!root.left && !root.right && root.val === sum) return true
  // 没有找到，则继续向下递归查找，左子树或右子树有一条符合条件便返回
  return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val)
}