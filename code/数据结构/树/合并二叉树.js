const mergeTrees = function (t1, t2) {
  // write code here
  if (t1 && t2) {
    t1.val += t2.val
    t1.left = mergeTrees(t1.left, t2.left)
    t1.right = mergeTrees(t1.right, t2.right)
  }
  // 如果t1为空，则返回t2，否则，返回t1
  return t1 || t2
}