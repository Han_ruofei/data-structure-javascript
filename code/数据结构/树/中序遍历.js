const midOrderTraversal = function (root) {
  const result = []
  const dfs = (root) => {
    if (!root) return
    dfs(root.left)
    result.push(root.val)
    dfs(root.right)
  }
  dfs(root)
  return result
}