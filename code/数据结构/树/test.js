const data = [
  {
    postName: '主任',
    userName: '王伟',
    sex: 'male'
  },
  {
    postName: '法师',
    userName: '王伟',
    sex: 'female'
  },
  {
    postName: '书记',
    userName: '王伟',
    sex: 'male'
  },
  {
    postName: '刺客',
    userName: '李白',
    sex: 'male'
  },
]

function setData (data) {
  let nameArr = []
  // 新的数据
  let newData = []
  for (let item of data) {
    // 如果名字数组里面没有当前名字
    if (!nameArr.includes(item.userName)) {
      // console.log('名字数组没有名字，执行添加')
      newData.push({
        userName: item.userName,
        postName: item.postName,
        sex: item.sex
      })
      nameArr.push(item.userName)
    } else {
      // console.log('当前名字已添加过', item.userName)
      let index = newData.findIndex(it => it.userName === item.userName)
      // console.log('在新数组中的index为', index)
      newData[index].postName = newData[index].postName + ' ' + item.postName
    }

  }
  // 删除数组
  nameArr.length = 0
  return newData
}
console.log(setData(data))