// function TreeNode (x) {
//   this.val = x
//   this.left = null
//   this.right = null
// }
// 使用中序遍历，遍历出结果，之后再对每一个节点做处理
function Convert (pRootOfTree) {
  // write code here
  const result = []
  const dfs = (root) => {
    if (!root) return
    dfs(root.left)
    result.push(root)
    dfs(root.right)
  }
  dfs(pRootOfTree)
  for (let i = 0; i < result.length; i++) {
    // 对第一个节点和最后一个节点做处理
    i === 0 ? result[i].left = null : result[i].left = result[i - 1]
    i === result.length - 1 ? result[i].right = null : result[i].right = result[i + 1]
  }
  return result[0]
}
