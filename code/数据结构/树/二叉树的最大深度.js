const maxDepth = function (root) {
  if (!root) return 0
  else {
    const left = maxDepth(root.left)
    const right = maxDepth(roog.right)
    return Math.max(left, right) + 1
  }
}