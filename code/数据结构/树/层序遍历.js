/*
 * function TreeNode(x) {
 *   this.val = x;
 *   this.left = null;
 *   this.right = null;
 * }
 */

/**
 *
 * @param root TreeNode类
 * @return int整型二维数组
 */
var levelOrder = function (root) {
  //二叉树的层序遍历
  let res = [], queue = []
  if (root === null) {
    return res
  }
  queue.push(root)
  while (queue.length !== 0) {
    // 记录当前层级节点数
    let length = queue.length
    //存放每一层的节点 
    let curLevel = []
    for (let i = 0; i < length; i++) {
      let node = queue.shift()
      curLevel.push(node.val)
      // 存放当前层下一层的节点
      node.left && queue.push(node.left)
      node.right && queue.push(node.right)
    }
    //把每一层的结果放到结果数组
    res.push(curLevel)
  }
  return res
}
const levelOrder2 = function (root) {
  const result = [], tempQueue = []
  if (!root) return result
  tempQueue.push(root)
  while (tempQueue.length != 0) {
    const len = tempQueue.length
    // 每一层节点的存储
    const curLevel = []
    for (let i = 0; i < len; i++) {
      const node = tempQueue.shift()
      curLevel.push(node.val)
      node.left && tempQueue.push(node.left)
      node.right && tempQueue.push(node.right)
    }
    result.push(curLevel)
  }
  return result
}
