/*
  给定一个二叉树，返回该二叉树的之字形层序遍历，
  （第一层从左向右，下一层从右向左，一直这样交替）
*/

const Print = function (root) {
  // 初始化result以及临时数组用于存储
  const result = [],
    tempQueue = []
  // 表示当前深度
  let depth = 0
  if (!root) return result
  tempQueue.push(root)
  while (tempQueue.length) {
    const len = tempQueue.length
    const curLevel = []
    const flag = depth % 2 === 0
    for (let i = 0; i < len; i++) {
      // 存数据时都是push进数组的，是有顺序的，先left再right，比如 2 4 6 8
      // 我们对数组是shift，也就是会先弹出2 之后在 4 6 8
      const node = tempQueue.shift()
      // 如果是偶数层，那么需要得到的结果为 2 4  6 8
      // 如果是偶数层，则需要得到的结果顺序为 8 6 4 2， 
      flag ? curLevel.push(node.val) : curLevel.unshift(node.val)
      node.left && tempQueue.push(node.left)
      node.right && tempQueue.push(node.right)
    }
    // 深度++
    depth++
    result.push(curLevel)
  }
  return result
}