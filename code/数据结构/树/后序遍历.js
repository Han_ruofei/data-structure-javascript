const postOrderTraversal = function (root) {
  const result = []
  const dfs = (root) => {
    if (!root) return
    dfs(root.left)
    dfs(root.right)
    result.push(root.val)
  }
  dfs(root)
  return result
}