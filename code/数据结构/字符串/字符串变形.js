/*
  对于一个长度为 n 字符串，我们需要对它做一些变形。
  首先这个字符串中包含着一些空格，就像"Hello World"一样，然后我们要做的是把这个字符串中由空格隔开的单词反序，同时反转每个字符的大小写。
  比如"Hello World"变形后就变成了"wORLD hELLO"。
*/

function trans (s, n) {
  //write code here
  return s.split(' ').reverse().map(item => {
    return item.split('').map(it => {
      // ascii码大于97则说明是小写
      return it = it.charCodeAt() >= 97 ? it.toUpperCase() : it.toLowerCase()
    }).join('')
  }).join(' ')
}

