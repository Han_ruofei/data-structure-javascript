/*
  给定两个数字字符串，返回相加后的结果,注意：如果直接转number可能长度不够
*/
// 思路：标志一个进位，将两个字符串的长度设置为一样，不够则用0补齐

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 * 计算两个数之和
 * @param s string字符串 表示第一个整数
 * @param t string字符串 表示第二个整数
 * @return string字符串
 */
function solve (s, t) {
  // write code here
  if (s.length === 0 || s == 0) return t
  if (t.length === 0 || t == 0) return s
  const maxLen = Math.max(s.length, t.length)
  s = s.padStart(maxLen, '0').split('').reverse()
  t = t.padStart(maxLen, '0').split('').reverse()
  let sum = ''
  let carry = 0
  for (let i = 0; i < s.length; i++) {
    let tmp = parseInt(s[i]) + parseInt(t[i]) + carry
    if (tmp >= 10) {
      sum += tmp % 10
      carry = Math.floor(tmp / 10)
    } else {
      sum += tmp
      carry = 0
    }
  }
  if (carry) sum += carry
  // console.log(sum.split('').reverse().join(''))
  return sum.split('').reverse().join('')
}
