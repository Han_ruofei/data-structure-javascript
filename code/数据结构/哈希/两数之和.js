// 力扣第一题，最初使用两重for循环就可以解决，
// 但是这里如果暴力解法就会超时，不能全部ac，因此采用哈希的方法
const twoSum = function (nums, target) {
  const map = new Map()
  let result = [-1, -1]
  for (let i = 0; i < nums.length; i++) {
    if (map.has(target - nums[i])) {
      result[0] = map.get(target - nums[i]) + 1
      result[1] = i + 1
    } else {
      map.set(nums[i], i)
    }
  }
  return result
}