function removeNthFromEnd (head, n) {
  // write code 

  let node = new ListNode(-1)
  node.next = head
  let pre = node
  let fast = slow = head
  // 让fast先跑n步
  for (let i = 0; i < n; i++) {
    if (fast === null) return null
    fast = fast.next
  }
  // 此时让fast和next同时跑
  while (fast) {
    fast = fast.next
    slow = slow.next
    pre = pre.next
  }
  // 删除slow节点，也就是要删除的节点
  pre.next = slow.next
  slow.next = null
  return node.next
}