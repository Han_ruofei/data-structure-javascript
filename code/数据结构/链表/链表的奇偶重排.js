/*
给定一个单链表，请设定一个函数，将链表的奇数位节点和偶数位节点分别放在一起，重排后输出。
注意是节点的编号而非节点的数值。
*/
const oddEvenList = function (head) {
  // 判断为空
  if (head === null) return head
  let node1 = new ListNode(-1), node2 = new ListNode(-1)
  let p1 = node1, p2 = node2
  // count记录奇偶位
  let count = 1
  while (head) {
    if (count++ % 2 === 0) {
      p1.next = head
      p1 = p1.next
    }
    else {
      p2.next = head
      p2 = p2.next
    }
    head = head.next
  }
  // 将偶号位链表末尾置空
  p1.next = null
  p2.next = node1.next
  console.log(node2.next)
  return node2.next
}