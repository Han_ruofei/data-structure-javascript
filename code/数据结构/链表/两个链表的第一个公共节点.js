// 又回到了交叉链表求交点的问题上，
// 当两条不属于同一条链表的指针指在一起时，则说明为相交节点
// 如何让他们相交，也就是让他们走相同的路程
// 让第一条链表走完又去走第二条链表，针对第二条链表也是同样的操作
const FindFirstCommonNode = function (head1, head2) {
  // 先判断非空
  if (!head1 || !head2) return null
  let p1 = head1, p2 = head2
  while (p1 != p2) {
    p1 = p1 === null ? head2 : p1.next
    p2 = p2 === null ? head1 : p2.next
  }
  return p1
}