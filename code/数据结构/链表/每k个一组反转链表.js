/*

描述
将给出的链表中的节点每 k 个一组翻转，返回翻转后的链表
如果链表中的节点数不是 k 的倍数，将最后剩下的节点保持原样
你不能更改节点中的值，只能更改节点本身。

数据范围： \ 0 \le n \le 2000 0≤n≤2000 ， 1 \le k \le 20001≤k≤2000 ，链表中每个元素都满足 0 \le val \le 10000≤val≤1000
要求空间复杂度 O(1)O(1)，时间复杂度 O(n)O(n)
例如：
给定的链表是 1\to2\to3\to4\to51→2→3→4→5
对于 k = 2k=2 , 你应该返回 2\to 1\to 4\to 3\to 52→1→4→3→5
对于 k = 3k=3 , 你应该返回 3\to2 \to1 \to 4\to 53→2→1→4→5

*/
/*
 * function ListNode(x){
 *   this.val = x;
 *   this.next = null;
 * }
 */

/**
  * 
  * @param head ListNode类 
  * @param k int整型 
  * @return ListNode类
  */
function reverseKGroup (head, k) {
  if (k === 1 || head === null) return head
  let arr = []
  let cur = head
  while (cur) {
    let temp = cur.next
    cur.next = null
    arr.push(cur)
    cur = temp
  }
  let newArr = cutArray(arr, k)

  newArr.forEach(item => {
    if (item.length === k) {
      item.reverse()
    }
  })
  let a = []
  newArr.forEach(items => {
    items.forEach(i => {
      a.push(i)
    })
  })
  for (let i = 0; i < a.length - 1; i++) {
    a[i].next = a[i + 1]
  }

  for (let i = 0; i < newArr.length - 1; i++) {
    newArr[i].next = newArr[i + 1]
  }
  return a[0]

  function cutArray (array, subLength) {
    let index = 0
    let newArr = []
    while (index < array.length) {
      newArr.push(array.slice(index, index += subLength))
    }
    return newArr
  }


}

let a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
const k = 3
const result = []
while (a.length > 0) {
  result.push(a.splice(0, k))
}
console.log(result)