// 方法一，使用哈希表对对遍历过的链表节点进行保存，
// 如果之前已经遍历过，那么就将它的值设置为true，
// 后续在进行判断时先进行判断，如果为true则直接返回，也就是之前访问过该节点了

const EntryNodeOfLoop = function (head) {
  if (!head || !head.next) return null
  let p = head
  // 使用map进行存储
  const map = new Map()
  while (p) {
    if (map.get(p)) return p
    else {
      map.set(p, true)
    }
    p = p.next
  }
}
// 方法二：（数学方法）
/*
  使用快慢指针的方法
*/
function EntryNodeOfLoop2 (pHead) {
  // 判断条件
  if (!pHead || !pHead.next) return null
  // 设置快慢指针
  let fast = slow = pHead
  while (fast && fast.next) {
    slow = slow.next
    fast = fast.next.next
    if (slow == fast) {
      let ptr = pHead
      while (ptr != slow) {
        ptr = ptr.next
        slow = slow.next
      }
      return ptr
    }
  }
}