// 方法一：第一遍计算链表长度，第二遍从头递进len-k个
function FindKthToTail (pHead, k) {
  // write code here
  let p = pHead
  // 计算整条链表的长度
  let count = 0
  while (p) {
    count++
    p = p.next
  }
  if (k > count) return null
  count = count - k
  p = pHead
  let c = 0
  while (p) {
    if (c === count) return p
    c++
    p = p.next

  }
}
// 例子：10个元素，选取倒数第三个元素
// 方法二：先让第一个人先跑 3 步，之后再和另一个在起点的人同时跑
// 当fast跑到终点时，slow则就跑到了倒数第三个的位置
const findKthtotail = function (head, k) {
  if (head === null) return null
  // 先让第一个人fast跑k步
  let fast = slow = head
  for (let i = 0; i < k; i++) {
    if (fast === null) return null
    fast = fast.next
  }
  // 再让slow和fast同时跑
  while (fast) {
    fast = fast.next
    slow = slow.next
  }
  return slow
}