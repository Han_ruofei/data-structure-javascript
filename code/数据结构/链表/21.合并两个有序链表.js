var mergeTwoLists = function (list1, list2) {
  // 创建的新链表的守卫节点
  const prehead = new ListNode(-1)

  // 让prev指针指向当前的节点，它的next即为返回链表的head
  let prev = prehead
  // 当list1和list2其中有一条为空时，就退出，将非空的那条添加在新链表之后
  while (list1 != null && list2 != null) {
    // 如果list1的值小于list2的值，那么则将list1节点添加，并将指针前进
    if (list1.val <= list2.val) {
      prev.next = list1
      list1 = list1.next
    } else {
      prev.next = list2
      list2 = list2.next
    }
    prev = prev.next
  }

  // 合并后 list1 和 list2 最多只有一个还未被合并完，我们直接将链表末尾指向未合并完的链表即可
  prev.next = list1 === null ? list2 : list1
  return prehead.next
}
