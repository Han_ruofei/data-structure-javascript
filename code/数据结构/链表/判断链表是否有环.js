/**
 * @param {ListNode} head
 * @return {boolean}
 */
// 使用快慢指针的思路进行环形链表的判断，如果相遇则说明有环，否则为无环
var hasCycle = function (head) {
  let fast = head, slow = head
  while (fast != null && fast.next != null) {
    fast = fast.next.next
    slow = slow.next
    if (slow === fast) {
      return true
    }
  }
  return false
}