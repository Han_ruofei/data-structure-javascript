/*
 * function ListNode(x){
 *   this.val = x;
 *   this.next = null;
 * }
 */

/**
  * 
  * @param head ListNode类 
  * @return ListNode类
  */
const deleteDuplicates = function (head) {
  //链表为空
  if (head === null) return head
  //链表不为空
  let p = head
  while (p.next != null) {
    if (p.val == p.next.val) {
      p.next = p.next.next  //删除重复元素
    } else {
      p = p.next //指针指向后移
    }
  }
  return head
}
