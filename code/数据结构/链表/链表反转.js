const reverseList = (head) => {
  if (head === null) return head
  let pre = null, cur = head
  while (cur) {
    let tmp = cur.next
    cur.next = pre
    pre = cur
    cur = tmp
  }
  return pre
}