// 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
/*
  输入: s = "abcabcbb"
  输出: 3 
  解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
*/
const lengthOfLongestSubstring = function (s) {
  const window = new Map()

  let left = right = 0
  let res = 0
  while (right < s.length) {
    const c = s[right]
    right++
    window.set(c, (window.get(c) || 0) + 1)
    while (window.get(c) > 1) {
      const d = s[left]
      left++
      window.set(d, window.get(d) - 1)
    }
    res = Math.max(res, right - left)
  }
  return res
}
