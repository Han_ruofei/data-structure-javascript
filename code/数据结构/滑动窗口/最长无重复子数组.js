/**
 * 
 * @param arr int整型一维数组 the array
 * @return int整型
 */
const maxLength = function (arr) {
  // write code here
  const window = new Map()
  let left = right = 0
  let len = 0
  // 开始扩大窗口
  while (right < arr.length) {
    const c = arr[right++]
    window.set(c, (window.get(c) || 0) + 1)
    // 开始缩小窗口
    while (window.get(c) > 1) {
      let d = arr[left++]
      window.set(d, window.get(d) - 1)
    }
    len = Math.max(len, right - left)
  }
  return len
}
