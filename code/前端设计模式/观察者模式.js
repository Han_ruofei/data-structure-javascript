// 观察者模式
/*

  例子：监控
  - 我们坐在教室里就是  被观察者
  - 监控后面的老师就是  观察者
  - 当观察者发现被观察者出发某些条件，观察者就会触发事件。
  - 比如被观察者在教室玩手机，观察者就会触发 请家长 技能

  观察者：
      每个观察者都有一个名字，一个技能
      被观察者具有的属性：
          - 状态
          - 当前观察他的人，观察他的人可能不止一个  []
          - 修改当前状态
          - 添加观察者
          - 移除观察者

*/

// 观察者 拥有名字以及技能
class Observer {
  // 当前观察者的名字以及技能
  constructor(name, fn) {
    this.name = name
    this.fn = fn
  }
}

// 被观察者
class Subject {

  constructor() {
    this.state = '学习'       //当前状态
    this.observers = []       //当前的观察者
  }

  setState (state) {            //修改当前的state
    this.state = state
    this.observers.forEach(item => {
      item.fn(this.state)
    })
  }
  addObserver (obs) {           //添加当前对象的观察者
    this.observers = this.observers.filter(item => item !== obs)
    this.observers.push(obs)
  }
  removeObserver (obs) {        //移除当前观察者
    this.observers = this.observers.filter(item => item !== obs)
  }
}

// 创建两个观察者
const bzr = new Observer('班主任', (reason) => { console.log(`因为${reason},触发请家长`) })
const xz = new Observer('校长', (reason) => { console.log(`因为${reason},触发找班主任`) })


// 创建一个被观察者
const zs = new Subject()
zs.addObserver(bzr)
zs.addObserver(xz)
zs.setState('玩手机')
