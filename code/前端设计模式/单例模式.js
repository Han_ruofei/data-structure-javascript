// 使用闭包的形式，将class类和之前的instance变量释放到内存中
// 这里的Person并不等于闭包里面的Person，这里只是个变量名字
const Person = (function () {
  class Person {
    constructor() {

    }
  }
  let instance = null
  return function singleTon () {
    if (!instance) instance = new Person()
    return instance

  }
})()
// 可以使用new也可以不使用
const p1 = new Person()
const p2 = new Person()
console.log(p1 === p2)