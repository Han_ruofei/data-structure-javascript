Array.prototype._filter = function (fn) {
  if (typeof (fn) !== 'function') return
  const result = []
  for (let i = 0; i < this.length; i++) {
    // filter得到的是数组中符合条件的值
    // 返回值是boolean类型，因此做判断，为true则添加
    if (fn(this[i], index, this)) {
      result.push(this[i])
    }
  }
  return result
}

let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
// console.log(arr._filter(item => item > 5))
// // 比较完善的写法
let newArr = arr.filter(function (item) {
  if (item > 5) return item
})
console.log(newArr)
