Array.prototype._map = function (fn) {
  // 传入的fn如果不是函数则直接返回
  if (typeof (fn) !== 'function') return
  // map方法会返回新数组
  const result = []
  // 给数组中的每一个方法都执行一次这个函数,并将返回值添加到result数组中
  // this指代的是当前调用的数组
  for (let i = 0; i < this.length; i++) {
    result.push(fn(this[i], i, this))
  }
  return result
}
let arr = [1, 2, 3, 4, 5, 6]
console.log(arr._map(item => item + 'aaa'))