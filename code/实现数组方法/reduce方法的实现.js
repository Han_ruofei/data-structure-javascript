// 回顾reduce方法
/*
  reduce数组方法接受两个参数
  第一个为回调函数fn，第二个参数为初始值
  array.reduce(function(total, currentValue, currentIndex, arr), initialValue)

*/
Array.prototype._reduce = function (fn, prev) {
  if (typeof (fn) !== 'function') return

  for (let i = 0; i < this.length; i++) {
    // 如果不传递prev，那么第一次调用时传递的初始参数就默认为数组的第一个元素，累加元素为数组索引为1处的元素
    if (prev === undefined) {
      prev = fn(this[i], this[i + 1], i + 1, this)
      // 此时i为1，但是下一次要从索引为2处开始加，所以这里i++
      i++
    } else {
      // 如果传递了初始参数，那么传递对应参数即可
      prev = fn(prev, this[i], i, this)
    }
  }
  // 最后返回prev，prev是一个元素，但不限类型，可能为数组，对象，数字等
  return prev
}
let arr = 'woainianizhidaoma'.split('')
console.log(arr._reduce((pre, cur) => {
  pre[cur] ? pre[cur]++ : pre[cur] = 1
  return pre
}, {}))

// let sum = arr.reduce((pre, cur, index, arr) => {
//   console.log(pre, cur, index, arr, '--reduce')
//   let result = pre + cur
//   return result
// })
// let sum2 = arr.reduce((pre, cur, index, arr) => {
//   console.log(pre, cur, index, arr, '---reduce')
//   let result = pre + cur
//   return result
// }, 0)
