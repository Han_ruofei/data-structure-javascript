
const _flat = (arr, depth) => {
  if (depth === 0 || !Array.isArray(arr)) return arr
  return arr.reduce((flatenArr, cur) => {
    return flatenArr.concat((Array.isArray(cur) && depth--) > 0 ? _flat(cur, depth) : cur)
  }, [])
}

const arr = [1, 2, 3, 4, [5, 6, 7, [8, 9, [10, 11, [12, 13]]]]]
console.log(_flat(arr, 2)
)