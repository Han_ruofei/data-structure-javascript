// 链表的存储是不连续的，它的前驱节点存储着下一个节点的内存地址
class ListNode {
  constructor(val, next) {
    this.val = (val === undefined ? 0 : val)
    this.next = (next === undefined ? null : next)
  }
}
const removeElements = function (head, val) {
  // 虚拟头节点，可以省略对head节点的判断
  const phead = new ListNode(0, head)
  // pre指针为cur的上一个指针
  let pre = phead, cur = head
  while (cur) {
    if (cur.val === val) {
      pre.next = cur.next
      cur = cur.next
      continue
    }
    cur = cur.next
    pre = pre.next
  }
  return phead.next
}