// 两条不同的链表，让他们走相同的路
/*
  链表A： a1->a2->
                    c1->c2->c3
          b1->b2-b3->
  我们需要返回的就是c1
  让p1和p2分别进行遍历，如果p1为空则让他等于headB，同样的让p2为headA
  相交时，p1分别遍历了：a1 a2 c1 c2 c3 b1 b2 b3 c1
         p2分别遍历了：b1 b2 b3 c1 c2 c3 a1 a2 c1
  此时则返回c1
  如果两条链表没有公共部分，那么指向最后p1==null,p2===null，也就跳出了循环，我们直接返回其中一个即可
*/
/**
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
var getIntersectionNode = function (headA, headB) {
  if (!headA || !headB) return null
  let p1 = headA, p2 = headB
  while (p1 !== p2) {
    p1 = p1 === null ? headB : p1.next
    p2 = p2 === null ? headA : p2.next
  }
  return p1
}