/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
// 有一些类似于反转链表，但是却不一样，需要我们注意换指向时的先后顺序
// 我们把链表可以想象成一条受重力影响的铁链，从head向下垂着，我们要想改变指向，必须先将底下的保存起来或者先指向底下的链子
// 否则当修改cur.next之后就找不到剩下的链表了
var swapPairs = function (head) {
  let virNode = new ListNode(-1, head)
  let tmp = virNode
  while (tmp.next && tmp.next.next) {
    let cur = tmp.next.next
    let pre = tmp.next
    pre.next = cur.next
    cur.next = pre
    tmp.next = cur
    // 这里要注意tmp赋值给pre，因为pre的指向已经改变，此时pre的next指向的是剩下未改动的
    tmp = pre
  }
  return virNode.next
}