/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
  const result = nums.reduce((pre, cur) => {
    pre[cur] ? pre[cur]++ : pre[cur] = 1
    return pre
  }, {})
  Object.values.sort((a, b) => a - b)
}
let arr = [1, 1, 1, 2, 2, 3]
topKFrequent(arr, 1)