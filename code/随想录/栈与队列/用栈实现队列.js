/*
  两个栈负责不同的内容，
  队列是先进先出，那么我们一个inStack负责输入，outStack负责输出
  队列的功能：push pop peek size isEmpty
  push:我们直接放入inStack重
  pop: 我们先将inStack中的元素出栈，再入栈到ouStack中，
  peek:返回栈顶最后一个元素
  isEmpty:当两个栈的长度都为0时返回true
*/

var MyQueue = function () {
  this.inStack = []
  this.outStack = []
}

/** 
* @param {number} x
* @return {void}
*/
MyQueue.prototype.push = function (x) {
  this.inStack.push(x)
}

/**
* @return {number}
*/
MyQueue.prototype.pop = function () {
  if (!this.outStack.length) {
    this.in2out()
  }
  return this.outStack.pop()
}

/**
* @return {number}
*/
MyQueue.prototype.peek = function () {
  if (!this.outStack.length) {
    this.in2out()
  }
  return this.outStack.slice(-1)[0]
}

/**
* @return {boolean}
*/
MyQueue.prototype.empty = function () {
  return this.outStack.length === 0 && this.inStack.length === 0
}
MyQueue.prototype.in2out = function () {
  while (this.inStack.length) {
    this.outStack.push(this.inStack.pop())
  }
}
/**
* Your MyQueue object will be instantiated and called as such:
* var obj = new MyQueue()
* obj.push(x)
* var param_2 = obj.pop()
* var param_3 = obj.peek()
* var param_4 = obj.empty()
*/