// 方法一：最简单的使用 indexOf,就可以直接返回
// 方法二：有点类似于固定窗口移动，直接将当前窗口的字符串与目标进行比较
var strStr = function (haystack, needle) {
  if (needle.length < haystack) return -1
  let left = 0, right = needle.length
  for (let i = 0; i <= haystack.length - needle.length; i++) {
    if (haystack[i] !== needle[0]) {
      left++
      right++
      continue
    }
    else {
      if (haystack.slice(left, right) === needle) return left
      else {
        left++
        right++
        continue
      }
    }
  }
  return -1
}