/*
  编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。
  不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
  eg:
  输入：s = ["h","e","l","l","o"]
  输出：["o","l","l","e","h"]
*/
// 方法一：使用reverse(),但这明显不是我们刷题的初衷
var reverseString = function (s) {
  return s.reverse()
}

// 方法二：使用两个指针，依次指向头和尾，分别交换他们两个所对应的元素的值
var reverseString2 = function (s) {
  let l = 0, r = s.length
  while (l++ < --r) [s[l], s[r]] = [s[r], s[l]]
};

