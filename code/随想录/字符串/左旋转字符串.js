/*
  字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。
  请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，
  该函数将返回左旋转两位得到的结果"cdefgab"。
*/


// 方法一：使用js自带的一些方法实现
var reverseLeftWords = function (s, n) {
  return s.slice(n).concat(s.slice(0, n))
}

// 方法二：
var reverseLeftWords2 = function (s, n) {
  /** Utils */
  function reverseWords (strArr, start, end) {
    let temp
    while (start < end) {
      temp = strArr[start]
      strArr[start] = strArr[end]
      strArr[end] = temp
      start++
      end--
    }
  }
  /** Main code */
  let strArr = s.split('')
  let length = strArr.length
  reverseWords(strArr, 0, length - 1)
  reverseWords(strArr, 0, length - n - 1)
  reverseWords(strArr, length - n, length - 1)
  return strArr.join('')
}