/*
  给定两个数组 nums1 和 nums2 ，返回 它们的交集 。输出结果中的每个元素一定是 唯一 的。我们可以 不考虑输出结果的顺序 。
  eg：
  输入：nums1 = [1,2,2,1], nums2 = [2,2]
  输出：[2]
*/
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersection = function (nums1, nums2) {
  // 数组去重
  // nums1 = [...new Set(nums1)],nums2 = [...new Set(nums2)]
  const map1 = new Map(), map2 = new Map(), result = []
  for (let i of nums1) map1.set(i, true)
  for (let j of nums2) map2.set(j, true)
  for (let i of nums1) {
    if (map2.get(i)) result.push(i)
  }
  return [...new Set(result)]
}