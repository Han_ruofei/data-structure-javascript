/*
  给定一个整数数组 nums 和一个整数目标值 target，
  请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
  eg:
  输入：nums = [2,7,11,15], target = 9
  输出：[0,1]
  解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 
  思路：使用map来存储之前已经遍历过的内容，
  之后再需要时如果从map中能直接get到，那么就可以直接返回
  也可以使用对象来实现

*/

var twoSum = function (nums, target) {
  const map = new Map()
  for (let i = 0; i < nums.length; i++) {
    if (map.has(target - nums[i])) return [i, map.get(target - nums[i])]
    else map.set(nums[i], i)
  }
  return []

}