/*
  给你四个整数数组 nums1、nums2、nums3 和 nums4 ，数组长度都是 n ，请你计算有多少个元组 (i, j, k, l) 能满足：
  0 <= i, j, k, l < n
  nums1[i] + nums2[j] + nums3[k] + nums4[l] == 0
*/


/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @param {number[]} nums3
 * @param {number[]} nums4
 * @return {number}
 */
var fourSumCount = function (nums1, nums2, nums3, nums4) {
  const twoSum = new Map()
  let count = 0
  // 将前两个数组所有和的结果存储起来
  for (let i of nums1) {
    for (let j of nums2) {
      let sum = i + j
      twoSum.set(sum, (twoSum.get(i + j) || 0) + 1)
    }
  }
  // 判断后面两个数组两个元素之和是否为 前面的负数，如果有则count++
  for (let i of nums3) {
    for (let j of nums4) {
      let sum = i + j
      count += (twoSum.get(0 - sum) || 0)
    }
  }
  return count
}