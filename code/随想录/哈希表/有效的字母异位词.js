/*
  给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。

    输入: s = "anagram", t = "nagaram"
    输出: true

    输入: s = "rat", t = "car"
    输出: false
  思路：与我们之前的滑动窗口有些类似，不过不一样。主要就是用哈希表来存储下每个字母出现的次数，
  如果每个字母出现的次数都相同，则说明是异位词
*/
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function (s, t) {
  if (s.length !== t.length) return false
  const sMap = new Map()
  const tMap = new Map()
  for (let i of s) sMap.set(i, (sMap.get(i) || 0) + 1)
  for (let it of t) {
    tMap.set(it, (tMap.get(it) || 0) + 1)
    if (tMap.get(it) > sMap.get(it)) return false
  }
  for (let j of s) {
    if (sMap.get(j) !== tMap.get(j)) return false
  }
  return true
}

// 方法二：
/*
  思路：
  定义一个数组叫做record用来上记录字符串s里字符出现的次数。
  需要把字符映射到数组也就是哈希表的索引下标上，因为字符a到字符z的ASCII是26个连续的数值，
  所以字符a映射为下标0，相应的字符z映射为下标25。
  再遍历 字符串s的时候，只需要将 s[i] - ‘a’ 所在的元素做+1 操作即可，并不需要记住字符a的ASCII，
  只要求出一个相对数值就可以了。 这样就将字符串s中字符出现的次数，统计出来了。
*/
var isAnagram2 = function (s, t) {
  if (s.length !== t.length) return false
  const resSet = new Array(26).fill(0)
  const base = "a".charCodeAt()
  for (const i of s) {
    resSet[i.charCodeAt() - base]++
  }
  for (const i of t) {
    if (!resSet[i.charCodeAt() - base]) return false
    resSet[i.charCodeAt() - base]--
  }
  return true
}