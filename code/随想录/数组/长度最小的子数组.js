/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen = function (target, nums) {
  let left = right = 0, len = Infinity, sum = 0
  while (right < nums.length) {
    const c = nums[right]
    console.log('当前right前进', 'left:', left, 'right:', right, 'sum:', sum + c)
    right++
    sum += c
    // 窗口满足条件开始缩小窗口
    while (sum >= target) {
      console.log(`当前窗口满足条件,当前sum：${sum},大于target:${target},开始缩小窗口`)
      const d = nums[left]
      if (sum - d < target) {
        // 更新len
        // len = right-left<len?right-left:len
        if (len > right - left) {
          console.log('当前len有变化，进行更新', '之前的len:', len, '更新后：', right - left)
          len = right - left
        }
      }
      left++
      console.log('当前left前进', 'left:', left, 'right:', right, 'sum:', sum - d)
      sum -= d
    }
  }
  return len === Infinity ? 0 : len
}