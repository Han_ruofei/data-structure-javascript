/* 
二分查找满足条件：
    有序排列
    无重复元素
注意事项：边界处理问题，有两种方案：[],(]
        两种方案的处理方式不同，需注意
newGet：
      如果需要除以2，则可以使用位运算，右移一位，比如5>>1就等于2，原理：0101>>1  0010
      如果不用的话，就需要使用 Math.floor((left+right)/2) 这种方法来取中间索引
*/
const search = (nums, target) => {
  let mid, left = 0, right = nums.length
  while (left < right) {
    // 这里可以使用位运算，也可以使用)left+right)/2
    // mid = left + Math.floor((right - left) >> 1) 
    mid = Math.floor((left + right) / 2)
    if (nums[mid] < target) {
      right = mid
    } else if (nums[mid] > target) {
      left = mid + 1
    } else {
      return mid
    }
  }
  return -1
}