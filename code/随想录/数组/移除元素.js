/*
  给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
  不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
  元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

  输入：nums = [3,2,2,3], val = 3
输出：2, nums = [2,2]
解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。
你不需要考虑数组中超出新长度后面的元素。
例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
*/

// 采用快慢指针的方法，快指针出去探路，慢指针用来替换当前元素
// 快指针每次都会向前进1格，慢指针则要在特殊条件下才会前进，当快指针指向的内容不是target时，就将慢指针指向的所替换，并++

const removeElement = function (nums, val) {
  let slow = fast = 0
  while (fast < nums.length) {
    if (nums[fast] !== val) nums[slow++] = nums[fast]
    fast++
  }
  return nums.slice(0, slow)
}

console.log(removeElement([1, 4, 6, 8, 2, 6, 2, 2, 1, 9, 2, 9], 2))