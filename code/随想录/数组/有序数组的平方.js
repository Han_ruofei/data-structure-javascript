
// 暴力法
var sortedSquares = function (nums) {
  const result = nums.map(item => item * item)
  return result.sort((a, b) => a - b)
}
// 双指针解法