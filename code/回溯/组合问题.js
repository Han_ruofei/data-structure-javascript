// 回溯算法的效率并不高，本质上还是暴力匹配
let result = []
let path = []
var combine = function (n, k) {

  result = []
  combineHelper(n, k, 1)

  return result
}
const combineHelper = (n, k, startIndex) => {
  if (path.length === k) {
    result.push([...path])
    return
  }
  for (let i = startIndex; i <= n - (k - path.length) + 1; ++i) {
    path.push(i)
    combineHelper(n, k, i + 1)
    path.pop()
  }
}
let strs = 'abc'
let ss = combine(strs.length, 3)
let srr = ss.map((item) => {
  let str = ''
  item.forEach(it => {
    str += strs[it - 1]
  })
  return str
})
console.log(srr)