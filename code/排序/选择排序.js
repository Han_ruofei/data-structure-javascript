// 选择排序
const selectSort = (arr) => {
  // 先选一个最小的，之后每一轮都跟这个最小的进行比较
  for (let i = 0; i < arr.length; i++) {
    let minIndex = i
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[minIndex]) {
        minIndex = j
      }
    }
    [arr[i], arr[minIndex]] = [arr[minIndex], arr[i]]
  }

}
let arr = [1, 3, 5, 2, 4, 8, 10, 9, 56, 22]
selectSort(arr)
console.log(arr)
// console.log(arr.sort(function (a, b) { return a - b }))
