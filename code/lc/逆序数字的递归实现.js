/**
 * 
 * @param {number} 传递的数字
 * @returns {string} 返回反转后的内容
 */
// const reverseNum = (n) => {
//   const num = Math.floor(n / 10)
//   if (!num) {
//     return n.toString()
//   } else {
//     return (n % 10).toString() + reverseNum(num)
//   }
// }
// console.log(reverseNum(12345))

const reverseNum = (n) => {
  const d = n % 10
  if (!n) {
    return n
  } else {
    return d.toString() + reverseNum(Math.floor(n / 10))
  }
}