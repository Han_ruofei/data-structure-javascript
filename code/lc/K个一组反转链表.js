/**
 * 
 * @param {ListNode} head 
 * @param {number} k 
 * @returns {ListNode} 
 */
// 指定区间反转,[head,stop)
function reverse (head, stop) {
  let pre = null, cur = head
  while (cur != stop) {
    let temp = cur.next
    cur.next = pre
    pre = cur
    cur = temp
  }
  return pre
}
const reverseKGroup = (head, k) => {
  if (head === null) return null
  let pre = cur = head
  for (let i = 0; i < k; i++) {
    if (cur === null) return head  // 如果剩余长度不够k，则直接返回head
    cur = cur.next    // cur一开始指向head，经历过k次迭代后指向第k+1个节点
  }
  const node = reverse(head, cur)  // 反转前k个节点
  pre.next = reverseKGroup(cur, k)  // 递归反转剩余的节点
  return newNode
}