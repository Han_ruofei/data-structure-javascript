/**
 * 387. 字符串中的第一个唯一字符
  给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。
  示例 1：
  输入: s = "leetcode"
  输出: 0
  示例 2:
  输入: s = "loveleetcode"
  输出: 2
  示例 3:

  输入: s = "aabb"
  输出: -1
 */



const firstUniqChar = function (s) {
  // 1. 统计每个字母出现的次数
  const charArr = s.split('')
  const countResult = charArr.reduce((pre, cur) => {
    pre[cur] ? pre[cur]++ : pre[cur] = 1
    return pre
  }, {})
  // 2. 遍历字符，如果有并且次数为1，则return
  for (let i = 0; i < charArr.length; i++) {
    if (countResult[charArr[i]] === 1) return i
  }
  return -1
}


console.log(firstUniqChar('leetcode'))
console.log(firstUniqChar('loveleetcode'))
console.log(firstUniqChar('aabb'))