function ReverseList (pHead) {
    // 链表的反转
    /*
    链表的反转，主要是将当前指向下一节点的指针指向上一个节点
    需要有三个指针
    第一个pre指针，这个指针初始化指为空
    第二个cur指针，这个指针指向当前的节点
    第三个tmp指针，用来保存剩余节点节点
    需要先将剩余节点使用tmp保存起来
    之后将当前节点指向pre
    再顺着链表移动pre以及cur指针
    */
    let pre = null
    let cur = pHead
    while (cur) {
        let tmp = cur.next
        cur.next = pre
        pre = cur
        cur = tmp
    }
    return pre
}
module.exports = {
    ReverseList
}