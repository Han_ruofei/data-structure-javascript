// 实现大数相加
const add = (a, b) => {
  a = a.toString()
  b = b.toString()
  let carry = 0
  let len = Math.max(a.length, b.length)
  let result = ''
  a.length < len ? a = a.padStart(len, '0') : b = b.padStart(len, '0')
  for (let i = len - 1; i >= 0; i--) {
    let sum = parseInt(a[i]) + parseInt(b[i]) + carry
    carry = Math.floor(sum / 10)
    other = sum % 10
    result = other + result
  }
  return carry > 0 ? carry + result : result
}
add(1, 9)