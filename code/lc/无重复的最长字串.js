
/**
 * 
 * @param {string} s 
 * @returns {number} 
 */
const lengthOfLongestSubstring = function (s) {
  const window = new Map()
  let left = right = 0
  let len = 0
  while (right < s.length) {
    const c = s[right]
    right++
    window.set(c, (window.get(c) || 0) + 1)
    while (window.get(c) > 1) {
      const d = s[left]
      left++
      window.set(d, window.get(d) - 1)
    }
    len = Math.max(len, right - left)
  }
  return len
}
console.log(lengthOfLongestSubstring("wasdfasasdf"))
