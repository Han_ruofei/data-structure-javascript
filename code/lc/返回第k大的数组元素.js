/*
给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。

请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。

你必须设计并实现时间复杂度为 O(n) 的算法解决此问题。
*/
var findKthLargest = function (nums, k) {
  // let pivot = nums.splice(Math.floor(nums.length / 2), 1)[0]
  // let left = []
  // let right = []
  // for (let i = 0; i < nums.length; i++) {
  //   if (nums[i] < pivot) {
  //     left.push(nums[i])
  //   }
  //   else {
  //     right.push(nums[i])
  //   }
  // }
  // findKthLargest(left).concat(pivot,findKthLargest(right))
  nums.sort(function (a, b) {
    return b - a
  })
  return nums[k - 1]
}
let arr = [1, 4, 5, 6, 2, 9, 10, 2, 6]
console.log(findKthLargest(arr, 2))