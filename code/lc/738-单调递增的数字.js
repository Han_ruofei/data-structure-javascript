const isVerify = (n) => {
  const arr = String(n).split('')
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] > arr[i + 1]) return false
  }
  return true
}

const monotoneIncreasingDigits = (n) => {
  if (isVerify(n)) return n
  else {
    return monotoneIncreasingDigits(n - 1)
  }
}
console.log(monotoneIncreasingDigits(10))
console.log(monotoneIncreasingDigits(1234))
console.log(monotoneIncreasingDigits(786))
console.log(monotoneIncreasingDigits(332))
console.log(monotoneIncreasingDigits(603253281))


function isVerify2 (n) {
  const arr = n.toString().split('')
  for (let i = 0; i < arr.length - 1; i++) {
    const result = arr.slice(i + 1).filter(item => item < arr[i])
    if (result.length > 0) return false
  }
  return true
}