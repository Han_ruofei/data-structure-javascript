const rl = require("readline").createInterface({ input: process.stdin })
const rows = []

rl.on("line", function (data) {
  if (data == '0') {
    let result = rows.map(num => {
      let count = 0
      while (num > 2) {
        count += ~~(num / 3)
        num = num % 3 + ~~(num / 3)
      }
      return num === 2 ? count + 1 : count
    })
    result.forEach(item => {
      console.log(item)
    })
  }
  else {
    rows.push(parseInt(data.trim()))
  }

})


