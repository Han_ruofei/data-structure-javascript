/*
描述
将一个节点数为 size 链表 m 位置到 n 位置之间的区间反转， 要求时间复杂度 O(n) O(n)， 空间复杂度 O(1) O(1)。
例如：
给出的链表为 1\ to 2\ to 3\ to 4\ to 5\ to NULL1→ 2→ 3→ 4→ 5→ NULL, m = 2, n = 4 m = 2, n = 4,
    返回 1\ to 4\ to 3\ to 2\ to 5\ to NULL1→ 4→ 3→ 2→ 5→ NULL.

数据范围： 链表长度 0 < size\ le 10000 < size≤ 1000， 0 < m\ le n\ le size0 < m≤ n≤ size， 链表中每个节点的值满足 | val | \le 1000∣ val∣≤ 1000
要求： 时间复杂度 O(n) O(n)， 空间复杂度 O(n) O(n)
进阶： 时间复杂度 O(n) O(n)， 空间复杂度 O(1) O(1)
*/
/*
function ListNode(x) {
    * this.val = x;
    * this.next = null;
    *
}
*/

function reverseBetween(head, m, n) {
    if (m === n) return false;
    //     设置哨兵节点，哨兵节点是为了处理边界问题而创建的，因此就不用考虑头节点为空以及从头节点开始插入的问题
    let top = new ListNode(0);
    top.next = head;
    let pre = top;
    let cur = head;
    for (let i = 0; i < m - 1; i++) {
        pre = pre.next
        cur = cur.next
    }
    for (let j = 0; j < n - m; j++) {
        let tmp = cur.next;
        cur.next = tmp.next;
        tmp.next = pre.next;
        pre.next = tmp;
    }
    return top.next;

}
module.exports = {
    reverseBetween: reverseBetween
};